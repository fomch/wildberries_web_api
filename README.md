# Открытое Web API Wildberries на Python

## Установка

```
pip install -U git+https://gitlab.com/fomch/wildberries_web_api
```

## Пример использования

```python
import wildberries_web_api

print(wildberries_web_api.get_supplier_id('<nmId>'))

# Получить позицию товара в поисковой выдаче
sp = wildberries_web_api.SearchPosition('Сорочка женская шелковая', '114606689')
print(sp.get())
```
