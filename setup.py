from setuptools import setup, find_packages

setup(
    name="wildberries_web_api",
    version="0.4",
    packages=find_packages(),
    description="WB Web API",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author="Fomchenkov Vyacheslav",
    author_email="fomchenkov.dev@gmail.com",
    url="https://gitlab.com/fomch/wildberries_web_api",
    install_requires=[
        "requests",
        "deprecated",
    ],
)
