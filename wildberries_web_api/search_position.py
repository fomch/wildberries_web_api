from time import sleep
from urllib.parse import urlencode

import requests


class SearchPosition:
	"""
	Получить позицию товара в поисковой выдаче на WB
	"""

	def __init__(self, word, article_wb):
		self.word = word
		self.article_wb = article_wb

	def get_position(
			self, 
			article: str,
			products: list[dict[str, any]],
			page: int) -> int or None: # type: ignore
		"""
		Ищет позицию товара в поисковой выдаче.

		Parameters:
		- article (str): Артикул товара, который нужно найти.
		- products (List[Dict[str, Any]]): Список товаров из поисковой выдачи.
		- page (int): Номер страницы поиска.

		Returns:
		- Optional[int]: Позиция товара, если найден, иначе None.
		"""
		for index, product in enumerate(products, start=(page - 1) * 100 + 1):
			if product.get('id') == int(article):
				return index
		return None

	def perform_wildberries_search(self, query: str, page: int = 1):
		"""
		Делает запрос к API Wildberries и возвращает данные в формате JSON.

		Parameters:
		- query (str): Строка запроса для поиска товара.
		- page (int, optional): Номер страницы поиска. По умолчанию 1.

		Returns:
		- Optional[Dict[str, Any]]: Словарь с данными, если запрос успешен, иначе None.
		# noqa
		"""
		params = {
			'appType': '1',
			'couponsGeo': '12,3,18,15,21',
			'curr': 'rub',
			'dest': '-1029256,-102269,-2162196,-1275608',
			'emp': '0',
			'lang': 'ru',
			'local': 'ru',
			'page': page,
			'pricemarginCoeff': '1.0',
			'query': query,
			'reg': '0',
			'regions': '80,64,83,4,38,33,70,68,69,86,75,30,40,48,1,22,66,31,71',
			'resultset': 'catalog',
			'sort': 'popular',
			'spp': '0',
			'suppressSpellcheck': False,
		}
		headers = {
			'Accept': '*/*',
			'Accept-Language': 'ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6',
			'Connection': 'keep-alive',
			'Origin': 'https://www.wildberries.ru',
			'Referer': 'https://www.wildberries.ru/catalog/0/search.aspx?' + urlencode({'search': query}),
			'Sec-Fetch-Dest': 'empty',
			'Sec-Fetch-Mode': 'cors',
			'Sec-Fetch-Site': 'cross-site',
			'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
			'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
			'sec-ch-ua-mobile': '?0',
			'sec-ch-ua-platform': '"Linux"',
		}
		url = 'https://search.wb.ru/exactmatch/ru/common/v4/search'

		res = requests.get(url, params=params, headers=headers)

		if res.status_code == 200:
			try:
				return res.json()
			except requests.exceptions.JSONDecodeError as e:
				print(f'Ошибка декодирования JSON:'
					f' {e}\nСодержимое ответа: {res.text}')
				return None
		else:
			print(f'Ошибка в запросе. Статус код: {res.status_code}')
			return None

	def search_on_pages(
			self, 
			query: str,
			article: str,
			max_pages=30,
			delay=3,
			max_retries=100):
		"""
			Поиск артикула на различных страницах результатов поиска.

			Parameters:
			- query (str): Строка запроса для поиска.
			- article (str): Артикул, который мы ищем.
			- max_pages (int, optional): Максимальное количество страниц для поиска. По умолчанию 30.
			- delay (int, optional): Задержка между запросами к API. По умолчанию 1 секунда.
			- max_retries (int, optional): Максимальное количество повторных попыток при ошибках запроса. По умолчанию 3.

			Yields:
			- tuple: Кортеж из данных ответа API и номера страницы, на которой был найден артикул.

			Если артикул не найден на всех страницах, возвращает (None, None).
		"""
		found_on_any_page = False
		found_on_page = False
		for page in range(1, max_pages + 1):
			retries = 0

			while retries < max_retries:
				try:
					response_data = self.perform_wildberries_search(query, page)
					sleep(delay)

					if response_data:
						try:
							products = response_data.get('data', {}).get('products', {})
						except:
							products = response_data.get('data', {})
						position = self.get_position(article, products, page)
						if position is not None:
							found_on_any_page = True
							found_on_page = True
							yield response_data, page, position
							break
						else:
							break
					else:
						print(f'Ошибка в запросе на страницу {page}.'
							f' Повторная попытка ({retries + 1}/{max_retries}).')
						retries += 1

					if retries == max_retries:
						print(f'Достигнуто максимальное количество попыток'
							f' для страницы {page}. Прекращаем поиск.')
						break

				except requests.exceptions.ConnectionError as e:
					print(f'Ошибка соединения: {e}. Повторная попытка ({retries + 1}/{max_retries}).')
					retries += 1
					sleep(delay)

		if not found_on_page and not found_on_any_page:
			yield '', None, '-'

	def get(self):
		for response_data, page, position in self.search_on_pages(
				self.word,
				self.article_wb,
				10,
				delay=1
			):
				# print(response_data)
				return position

		return None
