from concurrent.futures import ThreadPoolExecutor

import requests
from deprecated import deprecated


@deprecated
def get_seller_info(nmId):
    """
    Получить инфо о продавце по артикулу WB
    """

    nmId = str(nmId)

    for x in range(100):
        if x+1 < 10:
            basket_num = '0{}'.format(x+1)
        else:
            basket_num = str(x+1)

        url = 'https://basket-{}.wb.ru/vol{}/part{}/{}/info/sellers.json'.format(basket_num, nmId[:len(str(nmId))-5], nmId[:len(str(nmId))-3], nmId)
        response = requests.get(url)

        if response.status_code == 200:
            return response.json()

    return None


def get_supplier_id(nmId):
    """
    Получить ID продавца
    """

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'ru-RU,ru;q=0.9,en-GB;q=0.8,en;q=0.7,en-US;q=0.6',
        'Connection': 'keep-alive',
        'Origin': 'https://www.wildberries.ru' ,
        'Referer': 'https://www.wildberries.ru/catalog/65126333/detail.aspx',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'cross-site',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Chromium";v="124", "Google Chrome";v="124", "Not-A.Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Linux"',
    }
    url = f'https://card.wb.ru/cards/v2/detail?appType=1&curr=rub&dest=-3339992&spp=30&nm={nmId}'
    res = requests.get(url, headers=headers)
    return res.json()['data']['products'][0]['supplierId']


def get_product_photo(nmId, photo_size="big"):
    """
    Получить фото товара
    """

    nmId = str(nmId)

    def fetch_image(basket_num):
        if basket_num < 10:
            basket_num_str = '0{}'.format(basket_num)
        else:
            basket_num_str = str(basket_num)

        url = 'https://basket-{}.wbbasket.ru/vol{}/part{}/{}/images/{}/1.webp'.format(
            basket_num_str, nmId[:len(str(nmId))-5], nmId[:len(str(nmId))-3], nmId, photo_size,
        )
        try:
            response = requests.get(url)
        except requests.exceptions.ConnectionError:
            return None

        if response.status_code == 200:
            return url
        return None

    with ThreadPoolExecutor(max_workers=100) as executor:
        futures = [executor.submit(fetch_image, x+1) for x in range(100)]

    for future in futures:
        result = future.result()
        if result:
            return result

    return None


def get_card_data_with_spp(nmId):
    """
    Получить актуальные данные по карточке о скидке СПП
    """

    params = {
        'currency': 'RUB',
        'latitude': '55.753737',
        'longitude': '37.6201',
        'locale': 'ru',
        'address': 'Москва',
        'dt': 0,
    }
    url = 'https://user-geo-data.wildberries.ru/get-geo-info'
    res = requests.get(url, params=params)
    url = 'https://card.wb.ru/cards/v1/detail?' + res.json()['xinfo'] + '&nm=' + str(nmId)
    res = requests.get(url)
    return res.json()


def get_card_info(nmId):
    """
    Данные по карточке товара
    """

    nmId = str(nmId)

    for x in range(100):
        if x+1 < 10:
            basket_num = '0{}'.format(x+1)
        else:
            basket_num = str(x+1)

        url = 'https://basket-{}.wbbasket.ru/vol{}/part{}/{}/info/ru/card.json'.format(basket_num, nmId[:len(str(nmId))-5], nmId[:len(str(nmId))-3], nmId)
        response = requests.get(url)

        if response.status_code == 200:
            return response.json()

    return None


def tokens_introspect_v2(token):
    headers = {
        'accept': '*/*',
        'accept-language': 'en-US,en;q=0.9',
        'content-type': 'application/json',
        'origin': 'https://openapi.wildberries.ru',
        'priority': 'u=1, i',
        'referer': 'https://openapi.wildberries.ru/',
        'sec-ch-ua': '"Google Chrome";v="129", "Not=A?Brand";v="8", "Chromium";v="129"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Linux"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-site',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36',
        'x-introspect': token

    }
    url = "https://common-api.wildberries.ru/open-utils/tokens/introspect-v2"
    response = requests.get(url, headers=headers)
    return response.json()
