from .functions import *
from .search_position import *


__version__ = "0.4"
__author__ = "Fomchenkov Vyacheslav"
__email__ = "fomchenkov.dev@gmail.com"
__description__ = "WB Web API"
__url__ = "https://gitlab.com/fomch/wildberries_web_api"
